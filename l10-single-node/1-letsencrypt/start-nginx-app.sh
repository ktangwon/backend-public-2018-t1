#!/bin/bash
SITE=backend.gigadot.net

# Start nginx-proxy and letsencrypt-nginx-proxy-companion
docker run -d \
  --name nginx-app \
  --restart=always \
  -e "VIRTUAL_HOST=$SITE" \
  -e "LETSENCRYPT_HOST=$SITE" \
  nginx
