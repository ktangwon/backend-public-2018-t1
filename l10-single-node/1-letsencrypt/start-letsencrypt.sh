#!/bin/bash

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

# Start nginx-proxy and letsencrypt-nginx-proxy-companion
docker run -d -p 80:80 -p 443:443 \
  --name nginx-proxy \
  --restart=always \
  -v $DIR/app/certs:/etc/nginx/certs:ro \
  -v $DIR/app/vhost.d:/etc/nginx/vhost.d \
  -v $DIR/app/html:/usr/share/nginx/html \
  -v $DIR/app/log:/var/log/nginx/ \
  -v /var/run/docker.sock:/tmp/docker.sock:ro \
  --label com.github.jrcs.letsencrypt_nginx_proxy_companion.nginx_proxy \
  jwilder/nginx-proxy
  
docker run -d \
  --name letsencrypt \
  --restart=always \
  -v $DIR/app/certs:/etc/nginx/certs:rw \
  -v /var/run/docker.sock:/var/run/docker.sock:ro \
  --volumes-from nginx-proxy \
  jrcs/letsencrypt-nginx-proxy-companion
