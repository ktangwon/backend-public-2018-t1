#!/usr/bin/env python3
import grpc

import helloworld_pb2
import helloworld_pb2_grpc

URL='localhost:50051'

def demo():
    with grpc.insecure_channel(URL) as channel:
        hw_stub = helloworld_pb2_grpc.HelloWorldStub(channel)
        resp = hw_stub.SayHello(
            helloworld_pb2.HelloRequest(name='Weerapong',
                                        from_name='Kanat',
                                        magic=42))
    print('[#] Response was: {!r}'.format(resp))

if __name__ == '__main__':
    demo()
