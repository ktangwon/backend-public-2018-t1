package io.grpc.examples.helloworld;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;

import java.io.IOException;
import java.util.logging.Logger;

public class HelloWorldServer {
    private static final Logger logger = Logger.getLogger(HelloWorldServer.class.getName());

    private Server grpcServer;

    private void start() throws IOException {
        /* The port on which the server should run */
        int port = 50051;
        grpcServer = ServerBuilder.forPort(port)
                .addService(new HelloWorldImpl())
                .build()
                .start();
        logger.info("Server started, listening on " + port);
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            // Use stderr here since the logger may have been reset by its JVM shutdown hook.
            System.err.println("*** shutting down gRPC server since JVM is shutting down");
            HelloWorldServer.this.stop();
            System.err.println("*** server shut down");
        }));
    }

    private void stop() {
        if (grpcServer != null) {
            grpcServer.shutdown();
        }
    }

    /**
     * Await termination on the main thread since the grpc library uses daemon threads.
     */
    private void blockUntilShutdown() throws InterruptedException {
        if (grpcServer != null) {
            grpcServer.awaitTermination();
        }
    }

    /**
     * Main launches the server from the command line.
     */
    public static void main(String[] args) throws IOException, InterruptedException {
        final HelloWorldServer server = new HelloWorldServer();
        server.start();
        server.blockUntilShutdown();
    }

    static class HelloWorldImpl extends HelloWorldGrpc.HelloWorldImplBase {

        @Override
        public void sayHello(HelloRequest request, StreamObserver<HelloReply> responseObserer) {
            String message = String.format("Hello, %s from %s with magic %f",
                    request.getName(),
                    request.getFromName(),
                    request.getMagic());
            HelloReply response = HelloReply.newBuilder()
                    .setMessage(message)
                    .setToken(1409)
                    .build();
            responseObserer.onNext(response);
            responseObserer.onCompleted();
        }
    }
}
