#!/usr/bin/env python3

from concurrent import futures
import time

import grpc
import helloworld_pb2
import helloworld_pb2_grpc


class HelloWorld(helloworld_pb2_grpc.HelloWorldServicer):
    def SayHello(self, request, context):
        hello_msg = 'Hello, {} from {}'.format(request.name, request.from_name)
        return helloworld_pb2.HelloReply(message=hello_msg)

_ONE_HOUR = 60*60

def serve():
    print('[x] Server starting....')
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    helloworld_pb2_grpc.add_HelloWorldServicer_to_server(HelloWorld(), server)
    server.add_insecure_port('[::]:50051')
    server.start()
    try:
        while True:
            time.sleep(_ONE_HOUR)
    except KeyboardInterrupt:
        server.stop(0)
        print('[x] Server terminating....')

if __name__ == '__main__':
    serve()
