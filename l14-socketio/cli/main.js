let url = '/chat';
var socket = io(url);
var listenSocket = io('/');

listenSocket.on('reply', (data) => {
    let displayArea = $('#textarea');
    if (displayArea) {
        let time = new Date(data.timestamp);
        let fmtTime = time.toLocaleString('en-US');
        let who = data.name;
        let what = data.msg;
        displayArea
            .append(`<span class="timedisplay">[${fmtTime}]</span> `)
            .append(`<span class="senderdisplay">${who}</span> says `)
            .append(`<span class="msgdisplay">${what}</span><br />`);
    }
});

listenSocket.on('connect', () => {
    let displayArea = $('#textarea');
    if (displayArea) {
        displayArea.append('Connected to the shoutbox server... <br />');
    }
})
listenSocket.on('disconnect', () => {
    let displayArea = $('#textarea');
    if (displayArea) {
        displayArea.append('Server disconnected. <br/>');
    }
})

function send() {
    let name = $('#name').val();
    let msg = $('#msg').val();
    socket.emit('chat message', {msg, name});
}
