import os
import sys
from flask import Flask, request, Response
import rgrep

app = Flask(__name__)
base_path = os.getenv('GREP_PATH')

if not base_path:
    print('No grep path specified', file=sys.stderr)

@app.route('/grep')
def grep_get_request():
    query_string = request.args.get('query')
    if not query_string:
        return "No query string given", 400

    matched_lines = rgrep.all_text_grep(query_string, base_path)
    return Response(matched_lines, mimetype='text/plain')
