#!/usr/bin/env python3

from glob import glob
import sys

def all_text_grep(pattern, base_path='./'):
    for text_file in glob(base_path + '/**/*.txt'):
        with open(text_file, 'rt') as file_handle:
            for line in file_handle:
                if pattern in line:
                    line = line.strip()
                    yield '{text_file}: {line}\n'.format(
                        text_file=text_file, line=line)

if __name__ == '__main__':
    _, pattern, path, *_ = sys.argv
    for out_line in all_text_grep(pattern, path):
        print(out_line)
