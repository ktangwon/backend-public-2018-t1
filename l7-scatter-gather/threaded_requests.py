#!/usr/bin/env python3

import threading
import requests
from queue import SimpleQueue

class LineProxyTask(threading.Thread):
    def __init__(self, title, output, request_params):
        threading.Thread.__init__(self)
        self.title = title
        self.output = output
        self.request_params = request_params

    def run(self):
        u, kwargs = self.request_params
        response = requests.get(u, **kwargs, stream=True)
        for i, line in enumerate(response.iter_lines(), start=1):
            self.output.put(f'[{self.title}-{i}]... ' + line.decode()+'\n')


def serve_all(request_params):
    output = SimpleQueue()
    thrs = [
        LineProxyTask(
            u,
            output,
            (u, kwargs))
        for u, kwargs in request_params]

    def work_till_done():
        for thr in thrs:
            thr.start()
        for thr in thrs:
            thr.join()
        output.put(None)

    def retrieve_all_lines():
        while True:
            next_elem = output.get()
            if next_elem:
                yield next_elem
            else:
                break

    control_thread = threading.Thread(target=work_till_done)
    control_thread.setDaemon(True)
    control_thread.start()

    yield from retrieve_all_lines()

if __name__ == '__main__':
    demo_urls = [('https://nytimes.com', {}), ('https://cnn.com', {}), ('https://mahidol.ac.th', {})]
    for o in serve_all(demo_urls):
        print(">>", o)
