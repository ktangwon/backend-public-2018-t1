#!/usr/bin/env python3

from flask import Flask, request, Response
import threaded_requests

app = Flask(__name__)

target_workers = ['http://localhost:5000/grep', 'http://192.168.1.127:5000/grep']
# target_workers = ['http://localhost:5000/grep']
@app.route('/multigrep')
def multigrep_request():
    query_string = request.args.get('query')
    if not query_string:
        return "No query string given", 400
    requests = [(u, {'params': {'query': query_string}}) for u in target_workers]
    print(requests)
    return Response(response=threaded_requests.serve_all(requests),
                    mimetype='text/plain')

