from hello_world import incr

def test_incr_0():
    assert incr(1) == 2

def test_incr_1():
    assert incr(2) == 3
