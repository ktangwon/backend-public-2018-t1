import random
from locust import HttpLocust, TaskSet, task

class SimpleTasks(TaskSet):
    @task
    def get_status(self):
        self.client.get("/status")

    @task
    def post_vote(self):
        my_id = "{:04}".format(random.randint(0,9999))
        self.client.post(
            "/vote",
            json={'vote_for': '5981242', 'your_id': '568'+my_id})

class BasicBenchmark(HttpLocust):
    task_set = SimpleTasks
    host = 'http://127.0.0.1:5000'
