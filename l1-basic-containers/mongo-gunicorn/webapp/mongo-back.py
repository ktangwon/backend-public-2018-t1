from flask import Flask, jsonify, request
from pymongo import MongoClient

app = Flask(__name__)
client = MongoClient('db', 27017)
db = client['tally_db']
coll = db['total_tally']

@app.route("/status")
def get_status():
    entries = dict()
    for row in coll.find():
        entries[row['vote_for']] = row['count']
    return jsonify(entries)

@app.route("/vote", methods=['POST'])
def post_vote():
    body = request.json
    voter_id, vote_for_id = body.get('your_id'), body.get('vote_for')
    if voter_id and vote_for_id:
        coll.update(
            {'vote_for': vote_for_id},
            {'$inc': {'count': 1},
             '$set': {'vote_for': vote_for_id}
            },
            upsert=True)
        return jsonify({'status': 'OK'})
    else:
        response = jsonify({'status': 'ERROR'})
        response.status_code = 400
        return response
