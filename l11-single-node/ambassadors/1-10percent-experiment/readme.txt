kubectl create -f web-experiment.yaml
kubectl create configmap experiment-config --from-file=nginx.conf
kubectl create -f experiments-ambassador-pod.yaml
kubectl create -f experiments-service.yaml

kubectl delete -f experiments-service.yaml
kubectl delete -f experiments-ambassador-pod.yaml
kubectl delete configmap experiment-config
kubectl delete -f web-experiment.yaml
