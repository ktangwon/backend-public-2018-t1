kubectl create -f redis-shards.yaml
kubectl create -f redis-service.yaml
kubectl create configmap twem-config --from-file=nutcracker.yaml
kubectl create -f twemproxy-ambassador-pod.yaml

kubectl delete -f twemproxy-ambassador-pod.yaml
kubectl delete configmap twem-config
kubectl delete -f redis-service.yaml
kubectl delete -f redis-shards.yaml
