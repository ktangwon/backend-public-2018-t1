package io.muzoo.backend.spring.boot.kafka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class KafkaProducerController {

    @Autowired
    private KafkaService kafkaSenderService;

    @GetMapping("/kafka/{topicName}")
    public String producer(@PathVariable String topicName, @RequestParam String message) {
        kafkaSenderService.send(topicName, message);
        return "Message sent";
    }

}
