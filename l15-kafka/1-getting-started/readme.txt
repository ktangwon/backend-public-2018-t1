1. Download kafka
2. Start zookeeper
    ./bin/zookeeper-server-start.sh config/zookeeper.properties
    bin\windows\zookeeper-server-start.bat config\zookeeper.properties
3. Start kafka broker
    ./bin/kafka-server-start.sh config/server.properties
    bin\windows\kafka-server-start.bat config\server.properties
4. Create topic
    ./bin/kafka-topics.sh --zookeeper localhost:2181 --create --topic demo --partitions 1 --replication-factor 1
    bin\windows\kafka-topics.bat --zookeeper localhost:2181 --create --topic demo --partitions 1 --replication-factor 1
4. Start comsumer
    ./bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic demo
    bin\windows\kafka-console-consumer.bat --bootstrap-server localhost:9092 --topic demo
5. Start producer
    ./bin/kafka-console-producer.sh --broker-list localhost:9092 --topic demo
    bin\windows\kafka-console-producer.bat --broker-list localhost:9092 --topic demo
